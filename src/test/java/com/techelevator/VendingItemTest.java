package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VendingItemTest {

	private VendingItem chipVendingItem;
	private VendingItem stringTest;
	
	@Before
	public void setup() {
		//Arrange
		chipVendingItem = new VendingItem("A0", "SunChips - Harvest Cheddar", new DollarAmount (1970), 5);
		stringTest = new VendingItem("A0", "SunChips - Harvest Cheddar", new DollarAmount (1970), -1);

	
	
	}
	@Test
	public void test_initialization() {
		//Act
		String slot = chipVendingItem.getSlot();
		String name = chipVendingItem.getName();
		DollarAmount price = chipVendingItem.getPrice();
//		int 	quantity = chipVendingItem.getQuantity();
		
		//Assert
		Assert.assertEquals("A0", slot);
		Assert.assertEquals("SunChips - Harvest Cheddar", name);
		Assert.assertEquals(new DollarAmount(1970), price);
	}
	
	@Test
	public void set_quantity_test() {
		chipVendingItem.setQuantity(5);
		
		Assert.assertEquals(5, chipVendingItem.getQuantity());
	}
	@Test
	public void to_string_test() {
		chipVendingItem.toString();
		
		Assert.assertEquals(String.format("%-20.30s %-20.30s %-20.30s %-20.30s", "A0" , "SunChips - Harvest Cheddar", new DollarAmount(1970), (5 > 0 ? 5 : "Sold Out")), chipVendingItem.toString());
		
	}
	@Test
	public void to_string_test2() {

		stringTest.toString();
		
		
		Assert.assertEquals(String.format("%-20.30s %-20.30s %-20.30s %-20.30s", "A0" , "SunChips - Harvest Cheddar", new DollarAmount(1970), (-1 > 0 ? 5 : "Sold Out")), stringTest.toString());
	
	}
	
}
