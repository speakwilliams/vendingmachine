package com.techelevator;

public class VendingItem {
	
	private String slot;
	private String name;
	private DollarAmount price;
	private int quantity;

	public VendingItem(String slot, String name, DollarAmount price, int quantity) {
		this.slot = slot;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public String getSlot() {
		return slot;
	}

	public String getName() {
		return name;
	}

	public DollarAmount getPrice() {
		return price;
	}
	
	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return String.format("%-20.30s %-20.30s %-20.30s %-20.30s", slot , name, price, (quantity > 0 ? quantity : "Sold Out"));

	}

	
	
}
