package com.techelevator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class VendingMachine {

	private static Map<String, VendingItem> inventory = new HashMap<String, VendingItem>();
	private static Scanner input = new Scanner(System.in);

	public static void main (String[] args) {

		boolean masterWhile = true;
		while (masterWhile = true) {

			BufferedReader reader = null;

			try {
				File vendingDataFile = new File("./vendingmachine.csv"); 
				FileReader vendingDataReader = new FileReader(vendingDataFile);
				reader = new BufferedReader(vendingDataReader);

				String dataLine = reader.readLine();
				while(dataLine != null) {
					String[] items = dataLine.split("\\|");

					double priceDouble = Double.parseDouble(items[2]);
					int priceInPennies = (int) (priceDouble * 100);

					VendingItem item = new VendingItem(items[0], items[1], new DollarAmount (priceInPennies), 5);
					inventory.put(items[0], item);

					dataLine = reader.readLine();
				}

			} catch (FileNotFoundException e) {
				//		System.out.println("Failed to open to open file at " + filePath);
				System.exit(1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				//			fileInput.close();
				//			fileOutput.close();
				//			input.close();
			} 


			//MAIN MENU
			System.out.println("");
			System.out.println("-----------------------------------------------------------------------------------");
			System.out.println("");
			System.out.println("^_^ WELCOME TO THE VENDO-MATIC 500 ^_^");
			System.out.println("");
			System.out.println("");
			System.out.println("MAIN MENU");
			System.out.println("-----------------------------------------------------------------------------------");
			System.out.println("");
			System.out.println("");
			System.out.println("(1) Display Vending Machine Items");
			System.out.println("(2) Purchase");
			String mainMenuSelection = input.next();

			if(mainMenuSelection.equalsIgnoreCase("1")) {

				printItemList();

				//PURCHASE SELECTION
			} else if (mainMenuSelection.equalsIgnoreCase("2")) {
				DollarAmount currentMoney = new DollarAmount (0);
				boolean purchaseScreen = true;

				while (purchaseScreen = true) {

					System.out.println("");
					System.out.println("-----------------------------------------------------------------------------------");
					System.out.println("PURCHASE MENU");
					System.out.println("-----------------------------------------------------------------------------------");
					System.out.println("");
					System.out.println("");
					System.out.println("(1) Feed Money");
					System.out.println("(2) Select Product");
					System.out.println("(3) Finish Transaction");
					System.out.println("Current Money Provided: " + currentMoney);

					String purchaseMenuSelection = input.next();

					if ( purchaseMenuSelection.equalsIgnoreCase("1")) {

						System.out.println("How much do you want to deposit?");
						System.out.println("Please enter numerical value 1-5");
						System.out.println("(1) $1.00");
						System.out.println("(2) $2.00");
						System.out.println("(3) $5.00");
						System.out.println("(4) $10.00");
						System.out.println("(5) $20.00");

						int userDeposit = input.nextInt();
						String userInputDeposit = null;

						if (userDeposit == 1) {
							userInputDeposit = "1";
						} else if (userDeposit == 2) {
							userInputDeposit = "2";
						} else if (userDeposit == 3) {
							userInputDeposit = "5";
						} else if (userDeposit == 4) {
							userInputDeposit = "10";
						} else if (userDeposit == 5) {
							userInputDeposit = "20";
						} else {
							System.out.println("You have intered an invalid ammount.  Please enter a value 1 - 5");
						}

						double depositDouble = Double.parseDouble(userInputDeposit);
						int priceInPennies = (int) (depositDouble * 100);
						currentMoney = currentMoney.plus(new DollarAmount (priceInPennies));

					} else if (purchaseMenuSelection.equalsIgnoreCase("2")) {
						printItemList();

						System.out.println("");
						System.out.println("What item would you like?");
						System.out.println("example: A1");

						// accept the user response for what they want to buy
						String itemSelection = input.next().toUpperCase();
						// item selection response to user
						VendingItem item = inventory.get(itemSelection);

						
						if (inventory.containsKey(itemSelection) && item.getQuantity() > 0) {

							if (currentMoney.isGreaterThanOrEqualTo(item.getPrice())) {
								System.out.println("You have purchased: " + item.getName());
								item.setQuantity(item.getQuantity() - 1);


								PrintWriter fileOutput = null;
								BufferedWriter bw = null;
								FileWriter fw = null;

								try {
									fw = new FileWriter("TransactionLog.txt", true);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								bw = new BufferedWriter(fw);

								fileOutput = new PrintWriter(bw);
								fileOutput.printf("%-30.40s %-20.30s %-15.30s %-20.30s %-20.30s %n", new Date(), item.getName(), item.getSlot(), currentMoney, currentMoney.minus(item.getPrice()));

								fileOutput.flush();

								currentMoney = currentMoney.minus(item.getPrice());

							} else {							
								System.out.println("You do not have enough money to complete your transaction.  \nPlease add more funds.");
								purchaseScreen = false;
						
							}

						} else {
								System.out.println("You have selected an invalid or out of stock item. \nPlease choose another option.");
								purchaseScreen = false;
						}

					} else if (purchaseMenuSelection.equalsIgnoreCase("3")) {
						System.out.println("");
						System.out.println("-----------------------------------------------------------------------------------");
						System.out.println("Your transaction is complete.");
						System.out.println("-----------------------------------------------------------------------------------");

						System.out.println("Your change is: " + currentMoney);

						// convert dollar amount into pennies
						int cents = currentMoney.getCents();
						int dollars = currentMoney.getDollars() * 100;
						int pennies = cents + dollars;

						// convert pennies to smallest coin value	
						int quarters = pennies / 25;
						pennies %= 25;
						int dimes = pennies / 10;
						pennies %= 10;
						int nickels = pennies / 5;
						pennies %= 5;
						System.out.println("");
						System.out.println("Dispensed as:");
						System.out.println(String.format("Quarters = %d\nDimes = %d\nNickels = %d\nPennies = %d", quarters, dimes, nickels, pennies));
						System.out.println("");   
						System.out.println("Current Balance: " + currentMoney.minus(currentMoney));

						System.out.println("");
						System.out.println("-----------------------------------------------------------------------------------");
						System.out.println("Thank you for your business!");
						System.out.println("-----------------------------------------------------------------------------------");

						break;

					} else{
						System.out.println("You have made an invalid selection.  Please choose 1-3");
						purchaseScreen = true;
						masterWhile = false;

					}


				}// END PURCHASE SELECTION
			} else if (mainMenuSelection.equalsIgnoreCase("0")) {
				System.out.println("hidden reports menu");

			} else {
				System.out.println("Your have made an invalid selection.");
				System.out.println("Please select (1) or (2)");

			}

		} // end masterWhile


	} // end main

	private static void printItemList() {
		System.out.println("");
		System.out.println("-----------------------------------------------------------------------------------");

		String heading1 = "Slot";
		String heading2 = "Item";
		String heading3 = "Price";
		String heading4 = "Quantity";

		System.out.printf("%-20.30s %-20.30s %-20.30s %-20.30s %n",heading1, heading2, heading3, heading4);
		System.out.println("-----------------------------------------------------------------------------------");


		for(VendingItem item : inventory.values()) {
			System.out.println(item);
		}

	}

	//	private static void writeLog() {
	//		File transactionLog = null;
	//		BufferedWriter bw = null;
	//		PrintWriter fileOutput = null;
	//		
	//		try {
	//			FileWriter fw = new FileWriter("TransactionLog.txt", true);
	//		    bw = new BufferedWriter(fw);
	//
	//			fileOutput = new PrintWriter(bw);
	//			
	//
	//				fileOutput.println(item.getName());
	//			
	//			
	//			
	//				fileOutput.flush();
	//				System.out.println(transactionLog);
	//			
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		} finally {
	//			fileOutput.close();
	//		}
	//	}




} // end public class